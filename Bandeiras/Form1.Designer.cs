﻿namespace Bandeiras
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			this.dataGridView_ListaPaises = new System.Windows.Forms.DataGridView();
			this.textBox_PesquisaPaises = new System.Windows.Forms.TextBox();
			this.label_Line = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.label_NomePais = new System.Windows.Forms.Label();
			this.label_Capital = new System.Windows.Forms.Label();
			this.label_Populacao = new System.Windows.Forms.Label();
			this.label_Regiao = new System.Windows.Forms.Label();
			this.label_Moeda = new System.Windows.Forms.Label();
			this.label_TopLevelDomain = new System.Windows.Forms.Label();
			this.label_OnLine_OffLine = new System.Windows.Forms.Label();
			this.label_LoadDate = new System.Windows.Forms.Label();
			this.label_Results = new System.Windows.Forms.Label();
			this.button_fav = new System.Windows.Forms.Button();
			this.button_About = new System.Windows.Forms.Button();
			this.labelLoad = new System.Windows.Forms.Label();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.pictureBoxCapital = new System.Windows.Forms.PictureBox();
			this.pictureBoxDominio = new System.Windows.Forms.PictureBox();
			this.pictureBoxMoeda = new System.Windows.Forms.PictureBox();
			this.pictureBoxRegiao = new System.Windows.Forms.PictureBox();
			this.pictureBoxHabitantes = new System.Windows.Forms.PictureBox();
			this.button_Refresh = new System.Windows.Forms.Button();
			this.button_ShowFav = new System.Windows.Forms.Button();
			this.pictureBox_Bandeiras = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListaPaises)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxCapital)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxDominio)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxMoeda)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegiao)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxHabitantes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Bandeiras)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView_ListaPaises
			// 
			this.dataGridView_ListaPaises.AllowUserToAddRows = false;
			this.dataGridView_ListaPaises.AllowUserToDeleteRows = false;
			this.dataGridView_ListaPaises.AllowUserToResizeColumns = false;
			this.dataGridView_ListaPaises.AllowUserToResizeRows = false;
			this.dataGridView_ListaPaises.BackgroundColor = System.Drawing.Color.White;
			this.dataGridView_ListaPaises.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dataGridView_ListaPaises.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
			this.dataGridView_ListaPaises.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dataGridView_ListaPaises.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView_ListaPaises.ColumnHeadersVisible = false;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Silver;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridView_ListaPaises.DefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridView_ListaPaises.Location = new System.Drawing.Point(13, 51);
			this.dataGridView_ListaPaises.MultiSelect = false;
			this.dataGridView_ListaPaises.Name = "dataGridView_ListaPaises";
			this.dataGridView_ListaPaises.ReadOnly = true;
			this.dataGridView_ListaPaises.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			this.dataGridView_ListaPaises.RowHeadersVisible = false;
			this.dataGridView_ListaPaises.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.dataGridView_ListaPaises.Size = new System.Drawing.Size(310, 506);
			this.dataGridView_ListaPaises.TabIndex = 0;
			this.dataGridView_ListaPaises.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ListaPaises_CellClick);
			this.dataGridView_ListaPaises.SelectionChanged += new System.EventHandler(this.dataGridView_ListaPaises_SelectionChanged);
			// 
			// textBox_PesquisaPaises
			// 
			this.textBox_PesquisaPaises.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.textBox_PesquisaPaises.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox_PesquisaPaises.Location = new System.Drawing.Point(13, 12);
			this.textBox_PesquisaPaises.Name = "textBox_PesquisaPaises";
			this.textBox_PesquisaPaises.Size = new System.Drawing.Size(310, 24);
			this.textBox_PesquisaPaises.TabIndex = 1;
			this.toolTip1.SetToolTip(this.textBox_PesquisaPaises, "Search Bar");
			this.textBox_PesquisaPaises.TextChanged += new System.EventHandler(this.textBox_PesquisaPaises_TextChanged);
			// 
			// label_Line
			// 
			this.label_Line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label_Line.Location = new System.Drawing.Point(13, 39);
			this.label_Line.Name = "label_Line";
			this.label_Line.Size = new System.Drawing.Size(310, 2);
			this.label_Line.TabIndex = 2;
			// 
			// progressBar1
			// 
			this.progressBar1.BackColor = System.Drawing.Color.White;
			this.progressBar1.ForeColor = System.Drawing.Color.Silver;
			this.progressBar1.Location = new System.Drawing.Point(-2, 601);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(1335, 10);
			this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar1.TabIndex = 3;
			// 
			// label_NomePais
			// 
			this.label_NomePais.AutoSize = true;
			this.label_NomePais.Font = new System.Drawing.Font("Arial Black", 40F, System.Drawing.FontStyle.Bold);
			this.label_NomePais.Location = new System.Drawing.Point(364, 45);
			this.label_NomePais.Name = "label_NomePais";
			this.label_NomePais.Size = new System.Drawing.Size(213, 76);
			this.label_NomePais.TabIndex = 4;
			this.label_NomePais.Text = "NOME";
			// 
			// label_Capital
			// 
			this.label_Capital.AutoSize = true;
			this.label_Capital.Location = new System.Drawing.Point(415, 150);
			this.label_Capital.Name = "label_Capital";
			this.label_Capital.Size = new System.Drawing.Size(51, 13);
			this.label_Capital.TabIndex = 6;
			this.label_Capital.Text = "CAPITAL";
			// 
			// label_Populacao
			// 
			this.label_Populacao.AutoSize = true;
			this.label_Populacao.Location = new System.Drawing.Point(415, 182);
			this.label_Populacao.Name = "label_Populacao";
			this.label_Populacao.Size = new System.Drawing.Size(75, 13);
			this.label_Populacao.TabIndex = 6;
			this.label_Populacao.Text = "HABITANTES";
			// 
			// label_Regiao
			// 
			this.label_Regiao.AutoSize = true;
			this.label_Regiao.Location = new System.Drawing.Point(415, 213);
			this.label_Regiao.Name = "label_Regiao";
			this.label_Regiao.Size = new System.Drawing.Size(48, 13);
			this.label_Regiao.TabIndex = 6;
			this.label_Regiao.Text = "REGIÃO";
			// 
			// label_Moeda
			// 
			this.label_Moeda.AutoSize = true;
			this.label_Moeda.Location = new System.Drawing.Point(415, 245);
			this.label_Moeda.Name = "label_Moeda";
			this.label_Moeda.Size = new System.Drawing.Size(46, 13);
			this.label_Moeda.TabIndex = 6;
			this.label_Moeda.Text = "MOEDA";
			// 
			// label_TopLevelDomain
			// 
			this.label_TopLevelDomain.AutoSize = true;
			this.label_TopLevelDomain.Location = new System.Drawing.Point(415, 275);
			this.label_TopLevelDomain.Name = "label_TopLevelDomain";
			this.label_TopLevelDomain.Size = new System.Drawing.Size(54, 13);
			this.label_TopLevelDomain.TabIndex = 6;
			this.label_TopLevelDomain.Text = "DOMÍNIO";
			// 
			// label_OnLine_OffLine
			// 
			this.label_OnLine_OffLine.AutoSize = true;
			this.label_OnLine_OffLine.Location = new System.Drawing.Point(10, 583);
			this.label_OnLine_OffLine.Name = "label_OnLine_OffLine";
			this.label_OnLine_OffLine.Size = new System.Drawing.Size(31, 13);
			this.label_OnLine_OffLine.TabIndex = 6;
			this.label_OnLine_OffLine.Text = "LINE";
			this.toolTip1.SetToolTip(this.label_OnLine_OffLine, "Checks Internet Connection");
			// 
			// label_LoadDate
			// 
			this.label_LoadDate.AutoSize = true;
			this.label_LoadDate.Location = new System.Drawing.Point(51, 585);
			this.label_LoadDate.Name = "label_LoadDate";
			this.label_LoadDate.Size = new System.Drawing.Size(0, 13);
			this.label_LoadDate.TabIndex = 6;
			// 
			// label_Results
			// 
			this.label_Results.AutoSize = true;
			this.label_Results.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label_Results.Location = new System.Drawing.Point(10, 560);
			this.label_Results.Name = "label_Results";
			this.label_Results.Size = new System.Drawing.Size(37, 12);
			this.label_Results.TabIndex = 6;
			this.label_Results.Text = "Results";
			this.label_Results.Visible = false;
			// 
			// button_fav
			// 
			this.button_fav.BackColor = System.Drawing.Color.Transparent;
			this.button_fav.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.button_fav.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.button_fav.FlatAppearance.BorderSize = 0;
			this.button_fav.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.button_fav.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.button_fav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button_fav.ForeColor = System.Drawing.Color.Black;
			this.button_fav.Location = new System.Drawing.Point(816, 309);
			this.button_fav.Margin = new System.Windows.Forms.Padding(0);
			this.button_fav.Name = "button_fav";
			this.button_fav.Size = new System.Drawing.Size(81, 72);
			this.button_fav.TabIndex = 0;
			this.button_fav.TabStop = false;
			this.toolTip1.SetToolTip(this.button_fav, "Save as favorite");
			this.button_fav.UseVisualStyleBackColor = false;
			this.button_fav.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button1_MouseClick);
			// 
			// button_About
			// 
			this.button_About.BackColor = System.Drawing.Color.White;
			this.button_About.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.button_About.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
			this.button_About.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.button_About.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button_About.Location = new System.Drawing.Point(1000, 575);
			this.button_About.Name = "button_About";
			this.button_About.Size = new System.Drawing.Size(75, 23);
			this.button_About.TabIndex = 7;
			this.button_About.Text = "About..";
			this.button_About.UseVisualStyleBackColor = false;
			this.button_About.Click += new System.EventHandler(this.button1_Click);
			// 
			// labelLoad
			// 
			this.labelLoad.AutoSize = true;
			this.labelLoad.Font = new System.Drawing.Font("Arial Black", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelLoad.Location = new System.Drawing.Point(380, 566);
			this.labelLoad.Name = "labelLoad";
			this.labelLoad.Size = new System.Drawing.Size(0, 30);
			this.labelLoad.TabIndex = 8;
			// 
			// pictureBoxCapital
			// 
			this.pictureBoxCapital.Image = global::Bandeiras.Properties.Resources.capital;
			this.pictureBoxCapital.Location = new System.Drawing.Point(382, 143);
			this.pictureBoxCapital.Name = "pictureBoxCapital";
			this.pictureBoxCapital.Size = new System.Drawing.Size(28, 26);
			this.pictureBoxCapital.TabIndex = 9;
			this.pictureBoxCapital.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBoxCapital, "Capital");
			// 
			// pictureBoxDominio
			// 
			this.pictureBoxDominio.Image = global::Bandeiras.Properties.Resources.domain;
			this.pictureBoxDominio.Location = new System.Drawing.Point(380, 268);
			this.pictureBoxDominio.Name = "pictureBoxDominio";
			this.pictureBoxDominio.Size = new System.Drawing.Size(28, 26);
			this.pictureBoxDominio.TabIndex = 9;
			this.pictureBoxDominio.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBoxDominio, "Domain");
			// 
			// pictureBoxMoeda
			// 
			this.pictureBoxMoeda.Image = global::Bandeiras.Properties.Resources.coin;
			this.pictureBoxMoeda.Location = new System.Drawing.Point(380, 238);
			this.pictureBoxMoeda.Name = "pictureBoxMoeda";
			this.pictureBoxMoeda.Size = new System.Drawing.Size(28, 26);
			this.pictureBoxMoeda.TabIndex = 9;
			this.pictureBoxMoeda.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBoxMoeda, "Currency");
			// 
			// pictureBoxRegiao
			// 
			this.pictureBoxRegiao.Image = global::Bandeiras.Properties.Resources.world;
			this.pictureBoxRegiao.Location = new System.Drawing.Point(380, 206);
			this.pictureBoxRegiao.Name = "pictureBoxRegiao";
			this.pictureBoxRegiao.Size = new System.Drawing.Size(28, 26);
			this.pictureBoxRegiao.TabIndex = 9;
			this.pictureBoxRegiao.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBoxRegiao, "Region");
			// 
			// pictureBoxHabitantes
			// 
			this.pictureBoxHabitantes.Image = global::Bandeiras.Properties.Resources.population;
			this.pictureBoxHabitantes.Location = new System.Drawing.Point(380, 175);
			this.pictureBoxHabitantes.Name = "pictureBoxHabitantes";
			this.pictureBoxHabitantes.Size = new System.Drawing.Size(28, 26);
			this.pictureBoxHabitantes.TabIndex = 9;
			this.pictureBoxHabitantes.TabStop = false;
			this.toolTip1.SetToolTip(this.pictureBoxHabitantes, "Population");
			// 
			// button_Refresh
			// 
			this.button_Refresh.BackColor = System.Drawing.Color.Transparent;
			this.button_Refresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.button_Refresh.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.button_Refresh.FlatAppearance.BorderSize = 0;
			this.button_Refresh.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
			this.button_Refresh.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.button_Refresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button_Refresh.ForeColor = System.Drawing.Color.Black;
			this.button_Refresh.Image = global::Bandeiras.Properties.Resources.refresh;
			this.button_Refresh.Location = new System.Drawing.Point(333, 13);
			this.button_Refresh.Margin = new System.Windows.Forms.Padding(0);
			this.button_Refresh.Name = "button_Refresh";
			this.button_Refresh.Size = new System.Drawing.Size(24, 26);
			this.button_Refresh.TabIndex = 0;
			this.button_Refresh.TabStop = false;
			this.toolTip1.SetToolTip(this.button_Refresh, "Reload Countries");
			this.button_Refresh.UseVisualStyleBackColor = false;
			this.button_Refresh.Click += new System.EventHandler(this.button_Refresh_Click);
			// 
			// button_ShowFav
			// 
			this.button_ShowFav.BackColor = System.Drawing.Color.Transparent;
			this.button_ShowFav.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
			this.button_ShowFav.FlatAppearance.BorderColor = System.Drawing.Color.White;
			this.button_ShowFav.FlatAppearance.BorderSize = 0;
			this.button_ShowFav.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
			this.button_ShowFav.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
			this.button_ShowFav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button_ShowFav.ForeColor = System.Drawing.Color.Black;
			this.button_ShowFav.Image = global::Bandeiras.Properties.Resources.favUp;
			this.button_ShowFav.Location = new System.Drawing.Point(325, 40);
			this.button_ShowFav.Margin = new System.Windows.Forms.Padding(0);
			this.button_ShowFav.Name = "button_ShowFav";
			this.button_ShowFav.Size = new System.Drawing.Size(41, 40);
			this.button_ShowFav.TabIndex = 0;
			this.button_ShowFav.TabStop = false;
			this.toolTip1.SetToolTip(this.button_ShowFav, "Show Favorites");
			this.button_ShowFav.UseVisualStyleBackColor = false;
			this.button_ShowFav.Click += new System.EventHandler(this.button_ShowFav_Click);
			// 
			// pictureBox_Bandeiras
			// 
			this.pictureBox_Bandeiras.Location = new System.Drawing.Point(385, 309);
			this.pictureBox_Bandeiras.Name = "pictureBox_Bandeiras";
			this.pictureBox_Bandeiras.Size = new System.Drawing.Size(419, 248);
			this.pictureBox_Bandeiras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox_Bandeiras.TabIndex = 5;
			this.pictureBox_Bandeiras.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(1087, 605);
			this.Controls.Add(this.pictureBoxCapital);
			this.Controls.Add(this.pictureBoxDominio);
			this.Controls.Add(this.pictureBoxMoeda);
			this.Controls.Add(this.pictureBoxRegiao);
			this.Controls.Add(this.pictureBoxHabitantes);
			this.Controls.Add(this.labelLoad);
			this.Controls.Add(this.button_About);
			this.Controls.Add(this.button_ShowFav);
			this.Controls.Add(this.button_Refresh);
			this.Controls.Add(this.button_fav);
			this.Controls.Add(this.label_LoadDate);
			this.Controls.Add(this.label_Results);
			this.Controls.Add(this.label_OnLine_OffLine);
			this.Controls.Add(this.label_TopLevelDomain);
			this.Controls.Add(this.label_Moeda);
			this.Controls.Add(this.label_Regiao);
			this.Controls.Add(this.label_Populacao);
			this.Controls.Add(this.label_Capital);
			this.Controls.Add(this.pictureBox_Bandeiras);
			this.Controls.Add(this.label_NomePais);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.label_Line);
			this.Controls.Add(this.textBox_PesquisaPaises);
			this.Controls.Add(this.dataGridView_ListaPaises);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView_ListaPaises)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxCapital)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxDominio)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxMoeda)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxRegiao)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxHabitantes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox_Bandeiras)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox textBox_PesquisaPaises;
		private System.Windows.Forms.Label label_Line;
		private System.Windows.Forms.DataGridView dataGridView_ListaPaises;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label label_NomePais;
		private System.Windows.Forms.PictureBox pictureBox_Bandeiras;
		private System.Windows.Forms.Label label_Capital;
		private System.Windows.Forms.Label label_Populacao;
		private System.Windows.Forms.Label label_Regiao;
		private System.Windows.Forms.Label label_Moeda;
		private System.Windows.Forms.Label label_TopLevelDomain;
		private System.Windows.Forms.Label label_OnLine_OffLine;
		private System.Windows.Forms.Label label_LoadDate;
		private System.Windows.Forms.Label label_Results;
		private System.Windows.Forms.Button button_fav;
		private System.Windows.Forms.Button button_About;
		private System.Windows.Forms.Label labelLoad;
		private System.Windows.Forms.PictureBox pictureBoxHabitantes;
		private System.Windows.Forms.PictureBox pictureBoxCapital;
		private System.Windows.Forms.PictureBox pictureBoxRegiao;
		private System.Windows.Forms.PictureBox pictureBoxMoeda;
		private System.Windows.Forms.PictureBox pictureBoxDominio;
		private System.Windows.Forms.Button button_Refresh;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Button button_ShowFav;
	}
}

