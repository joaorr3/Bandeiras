﻿namespace Bandeiras.Servicos
{
	using Modelos;
	using Newtonsoft.Json;
	using System;
	using System.Collections.Generic;
	using System.Net.Http;
	using System.Threading.Tasks;

	public class ApiService //Esta classe contem apenas um método, e é responsável pela gestão de dados vindos da API.
	{
		public async Task<Response> GetData(string urlBase, string controller) //
		{
			try
			{
				var client = new HttpClient();
				client.BaseAddress = new Uri(urlBase);
				var response = await client.GetAsync(controller);
				var result = await response.Content.ReadAsStringAsync();

				if (!response.IsSuccessStatusCode)
				{
					return new Response
					{
						IsSuccess = false,
						Message = result,
					};
				}

				var ListaPais = JsonConvert.DeserializeObject<List<Pais>>(result);

				return new Response
				{
					IsSuccess = true,
					Result = ListaPais,
				};
			}
			catch (Exception ex)
			{
				return new Response
				{
					IsSuccess = false,
					Message = ex.Message,

				};
			}
		}
	}
}
