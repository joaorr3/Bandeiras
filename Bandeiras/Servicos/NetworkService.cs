﻿namespace Bandeiras.Servicos
{
	using Modelos;
	using System.Net;

	public class NetworkService //Esta classe faz um ping ao servidor da google para verificar se existe coneção à internet. Retorna um bool que é utilizados para gerir de onde são carregados os dados.
	{
		public Response CheckConnection()
		{
			var client = new WebClient();

			try
			{
				using (client.OpenRead("http://clients3.google.com/generate_204"))
				{
					return new Response
					{
						IsSuccess = true,
					};
				}
			}
			catch (System.Exception)
			{
				return new Response
				{
					IsSuccess = false,
					Message = "Check your Internet connection"
				};
			}
		}
	}
}
