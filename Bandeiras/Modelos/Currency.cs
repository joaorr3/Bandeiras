﻿using System.Collections.Generic;

namespace Bandeiras.Modelos
{
	public class Currency //A classe Currency faz a ponte com a API atraves das propriedades. A sua função é iniciar as propriedades de uma sublista.
	{
		public string code { get; set; }
		public string name { get; set; }
		public string symbol { get; set; }	
	}
}
