﻿using System.Collections.Generic;

namespace Bandeiras.Modelos
{
	public class Pais //A classe pais faz a ponte com a API atraves das propriedades.
	{
		public string name { get; set; }

		public string capital { get; set; }

		public string flag { get; set; }

		public int population { get; set; }

		public string region { get; set; }

		public List<Currency> currencies { get; set; }

		public List<string> topLevelDomain { get; set; }

		public int favorite { get; set; }

		public int idxPK { get; set; }
	}
}
