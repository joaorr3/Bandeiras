﻿using System;
using Bandeiras.Modelos;
using System.Collections.Generic;
using System.Windows.Forms;
using Bandeiras.Servicos;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Drawing.Imaging;
using System.Drawing;
using Bandeiras.Properties;
using System.Linq;

namespace Bandeiras
{
	public partial class Form1 : Form
	{
		#region Atributos
		private NetworkService networkService;
		private ApiService apiService;
		private DataService dataService;

		#endregion

		#region Propriedades
		Pais NewPais = new Pais();
		List<Pais> ListaPais = new List<Pais>();

		#endregion

		#region Variáveis Globais
		
		public bool select = true;

		public int selectRow = 0;

		public bool fillFav = true;

		public int idxDG;

		public string path;

		public int idxPK;

		public int idxPKaux;

		public string txtBox = "";

		public bool bttFavState = true;

		#endregion

		public Form1()
		{
			InitializeComponent();

			networkService = new NetworkService();
			apiService = new ApiService();
			dataService = new DataService();

			ShowIcon = false;


			EmptyLabels();
		}
		
		private void Form1_Load(object sender, EventArgs e)
		{
			textBox_PesquisaPaises.Select();

			dataGridView_ListaPaises.MouseWheel += new MouseEventHandler(dataGridView_ListaPaises_MouseWheel); //EventHandler para fazer scrol sem scrolBar

			label_OnLine_OffLine.Text = "Offline";
			label_OnLine_OffLine.ForeColor = Color.Red;
			button_fav.Image = Resources.Normal;


			LoadPaises();
		}

		#region LoadPaises
		//Metodo geral que verifica a conecção a internet e decide se os dados são inseridos na ListaPais atravez da internet ou da base de dados local.
		private async void LoadPaises()
		{
			bool load;
			
			var connection = networkService.CheckConnection();
			
			if (!connection.IsSuccess) //Apagar "!" (!connection.IsSuccess) para desligar a NET e testar DB
			{
				LoadLocalDB();
				load = false;
			}
			else
			{
				label_OnLine_OffLine.Text = "Online";
				label_OnLine_OffLine.ForeColor = Color.Green;

				await LoadAPI();
				load = true;
			}
			
			if (ListaPais.Count == 0)
			{
				label_LoadDate.Text = String.Format("No Internet connection and no local database found!");
				button_ShowFav.Enabled = false;
				return;
			}

			if (load)
			{
				label_LoadDate.Text = String.Format("Last Update at {0:F}", DateTime.Now);
			}
			else
			{
				label_LoadDate.Text = String.Format("Using Local Database");
			}

			RefreshDataGrid();

			EmptyLabels();
		}
		#endregion

		#region Load API
		//Insere os dados da API vindos da API, ordena por ordem alfabética ascendente, efetua o download das bandeiras, apaga todo o conteudo da directoria "\\Imagens\\SVG", 
		//invoca um metodo que apaga o conteudo das 3 tabelas e por ultimo reescreve a base de dados atravez do metodo SaveData.
		private async Task LoadAPI() 
		{
			var response = await apiService.GetData("https://restcountries.eu", "/rest/v2/all");

			ListaPais = (List<Pais>)response.Result;

			var orderedList = from Pais in ListaPais orderby Pais.name ascending select Pais;

			ListaPais = orderedList.ToList();

			foreach (var Pais in ListaPais)
			{
				if (Pais.name == "Åland Islands")
				{
					Pais.name = "Aland Islands";
				}
			}
			
			foreach (var Pais in ListaPais)
			{
				dataService.Downloadbandeiras(Pais.flag, Pais.name);
			}
			
			DirectoryInfo di = new DirectoryInfo(Application.StartupPath + "\\Imagens\\SVG");

			foreach (FileInfo file in di.GetFiles())
			{
				file.Delete();
			}

			dataService.DeleteData();

			dataService.SaveData(ListaPais, progressBar1, labelLoad);
		}
		#endregion

		#region Load Local DB
		//Insere os dados da API vindos da Base de Dados Local, ordena por ordem alfabética ascendente.
		private void LoadLocalDB() 
		{
			ListaPais = dataService.GetDataListaPais();

			var orderedList = from Pais in ListaPais orderby Pais.name ascending select Pais;

			ListaPais = orderedList.ToList();
		}
		#endregion

		#region Abilita o MouseWheel para fazer scroll sem scrol bar.
		private void dataGridView_ListaPaises_MouseWheel(object sender, MouseEventArgs e)
		{
			int currentIndex = this.dataGridView_ListaPaises.FirstDisplayedScrollingRowIndex;
			int scrollLines = SystemInformation.MouseWheelScrollLines;

			if (dataGridView_ListaPaises.RowCount > 0)
			{
				if (e.Delta > 0)
				{
					this.dataGridView_ListaPaises.FirstDisplayedScrollingRowIndex = Math.Max(0, currentIndex - scrollLines);
				}
				else if (e.Delta < 0)
				{
					this.dataGridView_ListaPaises.FirstDisplayedScrollingRowIndex = currentIndex + scrollLines;
				}



			}


		}
		#endregion

		#region Permiti arrastar o form sem control bar.
		protected override void WndProc(ref Message m)
		{
			base.WndProc(ref m);
			if (m.Msg == WM_NCHITTEST)
				m.Result = (IntPtr)(HT_CAPTION);
		}

		private const int WM_NCHITTEST = 0x84;
		private const int HT_CLIENT = 0x1;
		private const int HT_CAPTION = 0x2;

		#endregion

		#region Labels
		public void RefreshDataGrid() //Atualiza a DataGridView
		{
			dataGridView_ListaPaises.Rows.Clear();
			dataGridView_ListaPaises.Columns.Clear();

			dataGridView_ListaPaises.Columns.Add("ColPais", "");

			int idxlinha = 0;

			foreach (var Pais in ListaPais)
			{
				DataGridView linha = new DataGridView();
				dataGridView_ListaPaises.Rows.Add(linha);

				dataGridView_ListaPaises.Rows[idxlinha].Cells[0].Value = Pais.name;

				if (Pais.favorite == 1)
				{
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.White;
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.LightCoral;
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.LightCoral;

				}
				else
				{
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.Black;
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.White;
					dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.Silver;

				}

				idxlinha++;
			}

			dataGridView_ListaPaises.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			dataGridView_ListaPaises.RowHeadersVisible = false;

			dataGridView_ListaPaises.CurrentCell.Selected = false;
		}

		public void RefreshDataGridFav() //Actualizada a DataGridView se existirem favoritos dentro do modo favoritos.
		{
			dataGridView_ListaPaises.Rows.Clear();
			dataGridView_ListaPaises.Columns.Clear();

			dataGridView_ListaPaises.Columns.Add("ColPais", "");

			int idxlinha = 0;

			foreach (var Pais in ListaPais)
			{
				if (Pais.favorite == 1)
				{
					DataGridView linha = new DataGridView();
					dataGridView_ListaPaises.Rows.Add(linha);

					dataGridView_ListaPaises.Rows[idxlinha].Cells[0].Value = Pais.name;

					if (Pais.favorite == 1)
					{
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.LightCoral;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.White;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.IndianRed;

					}
					else
					{
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.Black;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.White;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.Silver;

					}

					idxlinha++;
				}
			}


			dataGridView_ListaPaises.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			dataGridView_ListaPaises.RowHeadersVisible = false;

			dataGridView_ListaPaises.CurrentCell.Selected = false;
			
		}

		public void RefreshInfoPaises() //Atualiza o conteudo de todas a labels
		{
			pictureBoxCapital.Visible = true;
			pictureBoxDominio.Visible = true;
			pictureBoxHabitantes.Visible = true;
			pictureBoxMoeda.Visible = true;
			pictureBoxRegiao.Visible = true;

			try
			{
				if (dataGridView_ListaPaises.RowCount > 0)
				{
					foreach (var Pais in ListaPais)
					{
						if (Pais.name == dataGridView_ListaPaises.CurrentRow.Cells[0].FormattedValue.ToString())
						{
							button_fav.Visible = true;
							label_NomePais.Text = Pais.name;
							label_Capital.Text = Pais.capital;
							label_Populacao.Text = Pais.population.ToString();
							label_Regiao.Text = Pais.region;
							foreach (var moeda in Pais.currencies)
							{
								label_Moeda.Text += moeda.name + "; ";
							}
							foreach (var Domain in Pais.topLevelDomain)
							{
								label_TopLevelDomain.Text += Domain + "; ";
							}
							path = Application.StartupPath + "\\Imagens\\PNG\\" + Pais.name + ".png";
							pictureBox_Bandeiras.Image = Image.FromFile(path);


							if (Pais.favorite == 0)
							{
								button_fav.Image = Resources.Normal;
							}
							else
							{
								button_fav.Image = Resources.Mouse_Down;
							}
						}
					}
				}
			}
			catch
			{
				MessageBox.Show("Can't display some content");
			}
		}

		private void EmptyLabels() //Apaga o conteudo de todas a labels
		{
			label_NomePais.Text = "";
			label_Capital.Text = "";
			label_Populacao.Text = "";
			label_Regiao.Text = "";
			label_Moeda.Text = "";
			label_TopLevelDomain.Text = "";
			pictureBox_Bandeiras.Image = null;
			button_fav.Visible = false;

			pictureBoxCapital.Visible = false;
			pictureBoxDominio.Visible = false;
			pictureBoxHabitantes.Visible = false;
			pictureBoxMoeda.Visible = false;
			pictureBoxRegiao.Visible = false;
		}
		#endregion

		#region Events

		private void textBox_PesquisaPaises_TextChanged(object sender, EventArgs e)
		{
			#region Refresh DataGrid

			
			dataGridView_ListaPaises.Rows.Clear();
			dataGridView_ListaPaises.Columns.Clear();

			dataGridView_ListaPaises.Columns.Add("ColPais", "");

			int idxlinha = 0;

			
			foreach (var Pais in ListaPais)
			{
				if (Pais.name.ToString().ToLower().StartsWith(textBox_PesquisaPaises.Text.ToLower().ToString()))
				{
					DataGridView linha = new DataGridView();
					dataGridView_ListaPaises.Rows.Add(linha);

					dataGridView_ListaPaises.Rows[idxlinha].Cells[0].Value = Pais.name;

					if (Pais.favorite == 1)
					{
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.White;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.LightCoral;
					}
					else
					{
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.Black;
						dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.White;
					}

					idxlinha++;
				}
			}

			dataGridView_ListaPaises.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
			dataGridView_ListaPaises.RowHeadersVisible = false;

			#endregion

			dataGridView_ListaPaises.ClearSelection();

			if (textBox_PesquisaPaises.Text == "")
			{
				label_Results.Visible = false;
			}
			else
			{
				label_Results.Visible = true;
			}

			txtBox = textBox_PesquisaPaises.Text;

			if (dataGridView_ListaPaises.RowCount > 0)
			{
				foreach (var Pais in ListaPais)
				{				
					if (Pais.name == dataGridView_ListaPaises.CurrentRow.Cells[0].FormattedValue.ToString())
					{
						idxPK = Pais.idxPK - 1;
					}				
				}
			}

			label_Results.Text = dataGridView_ListaPaises.RowCount.ToString() + " Results";

			EmptyLabels();

		} //Responsável pela pesquisa.
		
		private void dataGridView_ListaPaises_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			idxDG = dataGridView_ListaPaises.CurrentRow.Index;

			EmptyLabels();
			RefreshInfoPaises();
		} //Altera o conteúdo das labels atraves de um click.

		private void dataGridView_ListaPaises_SelectionChanged(object sender, EventArgs e)
		{
			EmptyLabels();
			RefreshInfoPaises();
		} //Altera o conteúdo das labels atraves das setas.

		private void button1_MouseClick(object sender, MouseEventArgs e)
		{								
			foreach (var Pais in ListaPais)
			{
				if (Pais.name == dataGridView_ListaPaises.CurrentRow.Cells[0].FormattedValue.ToString())
				{

					if (Pais.favorite == 0)
					{
						button_fav.Image = Resources.Mouse_Down;
						dataService.SetFav(1, Pais.name);
						Pais.favorite = 1;

						dataGridView_ListaPaises.CurrentCell.Style.ForeColor = Color.White;
						dataGridView_ListaPaises.CurrentCell.Style.BackColor = Color.LightCoral;
						dataGridView_ListaPaises.CurrentCell.Style.SelectionBackColor = Color.LightCoral;
					}
					else
					{
						button_fav.Image = Resources.Normal;
						dataService.SetFav(0, Pais.name);
						Pais.favorite = 0;

						dataGridView_ListaPaises.CurrentCell.Style.ForeColor = Color.Black;
						dataGridView_ListaPaises.CurrentCell.Style.BackColor = Color.White;
						dataGridView_ListaPaises.CurrentCell.Style.SelectionBackColor = Color.Silver;
					}
				}
			}

			LoadLocalDB();
			//dataService.GetDataListaPais();
			//RefreshDataGridBtt();
		} //Marca o país como favorito.

		private void button1_Click(object sender, EventArgs e)
		{

			MessageBox.Show("Author: João Ribeiro v1.2.4", "About");

		} //Mostra a info de desenvolvimento.

		private void button_Refresh_Click(object sender, EventArgs e)
		{
			dataService.OpenConnection();

			LoadPaises();
		} //Permite fazer reload da API para verificar se existem alterações nos dados sem reiniciar o programa.

		private void button_ShowFav_Click(object sender, EventArgs e)
		{
			foreach (var fav in ListaPais)
			{
				if (fav.favorite == 0)
				{
					dataGridView_ListaPaises.Rows.Clear();
					dataGridView_ListaPaises.Columns.Clear();
				}
			}
			
			if (bttFavState == true)
			{
				button_ShowFav.Image = Resources.favDwn;
				bttFavState = false;
				
				foreach (var fav in ListaPais)
				{
					if (fav.favorite == 1)
					{
						RefreshDataGridFav();
					}
				}
			}
			else
			{
				button_ShowFav.Image = Resources.favUp;
				bttFavState = true;
				RefreshDataGrid();
			}

			EmptyLabels();
		} //Permite entrar no modo favoritos, que mostra todos os paises definidos como favoritos.

		#endregion

		#region Not in use
		//public void RefreshDataGridBtt() //Not in use
		//{
		//int idxlinha = 0;			

		//if (txtBox == textBox_PesquisaPaises.Text)
		//{
		//	idxPKaux = idxPK;
		//}

		//foreach (DataGridViewRow Row in dataGridView_ListaPaises.Rows)
		//{			
		//	if (ListaPais[idxPKaux].favorite == 1)
		//	{
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.White;
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.LightCoral;
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.LightCoral;
		//	}
		//	else
		//	{
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.ForeColor = Color.Black;
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.BackColor = Color.White;
		//		dataGridView_ListaPaises.Rows[idxlinha].DefaultCellStyle.SelectionBackColor = Color.Silver;
		//	}

		//	idxPKaux++;
		//	idxlinha++;
		//}
		//}
		#endregion

	}
}
