﻿namespace Bandeiras.Modelos
{
	public class Response //Cria as propriedades utilizadas nos serviços.
	{
		public bool IsSuccess { get; set; }

		public string Message { get; set; }

		public object Result { get; set; }

	}
}
