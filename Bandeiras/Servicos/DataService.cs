﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using Bandeiras.Modelos;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using ImageMagick;

namespace Bandeiras.Servicos
{
	public class DataService //A classe DataServide gere toda a mecânica da base de dados local.
	{
		private SQLiteConnection connection;

		private SQLiteCommand command;

		private DialogService dialogService;

		Currency Currency = new Currency();

		#region Variáveis Globais

		public int PKInc = 1;

		public int fav = 0;

		#endregion

		public DataService() // Cria a base de dados.
		{

			dialogService = new DialogService();

			if (!Directory.Exists("Database"))
			{
				Directory.CreateDirectory("Database");

			}

			var path = @"Database\ListaPais.sqlite";


			try
			{
				connection = new SQLiteConnection("Data Source=" + path);
				connection.Open();


				string sqlCommand = "create table if not exists ListaPais(id_pais int, name_pais varchar(32), capital_pais varchar(32), flag_pais varchar(32), population_pais int, region_pais varchar(32), pais_fav int default 0)";

				command = new SQLiteCommand(sqlCommand, connection);

				command.ExecuteNonQuery();


				string sqlCommand01 = "create table if not exists ListaMoeda(id_pais int, code_moeda varchar(32), name_moeda varchar(32), symbol_moeda varchar(32))";

				command = new SQLiteCommand(sqlCommand01, connection);

				command.ExecuteNonQuery();


				string sqlCommand02 = "create table if not exists ListaDominio(id_pais int, dominio varchar(32))";

				command = new SQLiteCommand(sqlCommand02, connection);
				
				command.ExecuteNonQuery();

			}
			catch (Exception ex)
			{

				dialogService.ShowMessage("Erro: DS_01", ex.Message);
			}
		}

		public void OpenConnection() //Abre a conecção à base de dados.
		{
			connection.Open();
		}

		public void SaveData(List<Pais> ListaPaisDS, ProgressBar pbar, Label lbLoad)//Grava os dados na base de dados.
		{
			try
			{
				pbar.Maximum = 260;
				pbar.Value = 0;
				
				foreach (var PaisDS in ListaPaisDS)
				{
					if (PaisDS.name == "Åland Islands") PaisDS.name = "Aland Islands";
					
					string sqlPais = string.Format("insert into ListaPais(id_pais, name_pais, capital_pais, flag_pais, population_pais, region_pais, pais_fav) values(@id_pais, @name_pais, @capital_pais, @flag_pais, @population_pais, @region_pais, @pais_fav)");

					command = new SQLiteCommand(sqlPais, connection);

					command.Parameters.AddWithValue("@id_pais", PKInc);
					command.Parameters.AddWithValue("@name_pais", PaisDS.name);
					command.Parameters.AddWithValue("@capital_pais", PaisDS.capital);
					command.Parameters.AddWithValue("@flag_pais", PaisDS.flag);
					command.Parameters.AddWithValue("@population_pais", PaisDS.population);
					command.Parameters.AddWithValue("@region_pais", PaisDS.region);
					command.Parameters.AddWithValue("@pais_fav", fav);


					command.ExecuteNonQuery();
					
					foreach (var currency in PaisDS.currencies)
					{
						
						if (currency.code == null) currency.code = "n/a";

						if (currency.name == null) currency.name =  "n/a";

						if (currency.symbol ==null) currency.symbol =  "n/a";

						
						string sqlCurrencies = string.Format("insert into ListaMoeda(id_pais, code_moeda, name_moeda, symbol_moeda) values(@id_pais, @code_moeda, @name_moeda, @symbol_moeda)");

						command = new SQLiteCommand(sqlCurrencies, connection);

						command.Parameters.AddWithValue("@id_pais", PKInc);
						command.Parameters.AddWithValue("@code_moeda", currency.code);
						command.Parameters.AddWithValue("@name_moeda", currency.name);
						command.Parameters.AddWithValue("@symbol_moeda", currency.symbol);
						
						command.ExecuteNonQuery();
					}

					foreach (var dominio in PaisDS.topLevelDomain)
					{
						string sqlDominio = string.Format("insert into ListaDominio(id_pais, dominio) values(@id_pais, @dominio)");

						command = new SQLiteCommand(sqlDominio, connection);

						command.Parameters.AddWithValue("@id_pais", PKInc);
						command.Parameters.AddWithValue("@dominio", dominio);

						command.ExecuteNonQuery();
					}

					lbLoad.Text = PaisDS.name;
					lbLoad.Update();

					pbar.Value++;
					PKInc++;
				}

				connection.Close();
				pbar.Value = 260;
				lbLoad.Visible = false;
				//pbar.Visible = false;
			}
			catch (Exception ex)
			{
				dialogService.ShowMessage("Erro: DS_02", ex.Message);
			}

		}

		public void Downloadbandeiras(string link, string name) //Faz o download das bandeiras e a conversão para o formato adequado.
		{
			try
			{
				if (!Directory.Exists("Imagens\\SVG"))
				{
					Directory.CreateDirectory("Imagens\\SVG");
				}
				var SVG = Directory.GetFiles(Application.StartupPath + "\\Imagens\\SVG");

				if (!Directory.Exists("Imagens\\PNG"))
				{
					Directory.CreateDirectory("Imagens\\PNG");
				}
				var PNG = Directory.GetFiles(Application.StartupPath + "\\Imagens\\PNG");

				if (PNG.Length < 250)
				{
					using (WebClient webClient = new WebClient())
					{
						webClient.DownloadFile(link, Application.StartupPath + "\\Imagens\\SVG\\" + name + ".svg");
					}					
				}

				if (PNG.Length < 250)
				{
					using (MagickImage flag = new MagickImage(Application.StartupPath + "\\Imagens\\SVG\\" + name + ".svg"))
					{
						flag.Write(Application.StartupPath + "\\Imagens\\PNG\\" + name + ".png");
					}
				}
			}
			catch (Exception ex)
			{

				MessageBox.Show("Error: Downloading Flags", ex.Message);
			}
			

		}

		public void SetFav(int fv, string namePais) //Faz a query que define se o país é favorito.
		{
			connection.Open();

			string sqlFav = string.Format("UPDATE ListaPais SET pais_fav = @fav WHERE name_pais = @name");

			command = new SQLiteCommand(sqlFav, connection);

			command.Parameters.AddWithValue("@fav", fv);
			command.Parameters.AddWithValue("@name", namePais);

			try
			{
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{

				MessageBox.Show("Erro: Set_Fav "+ ex.Message);
			}
		}

		public List<Pais> GetDataListaPais() //Insere na GetListPaisDS os dados existentes na base de dados.
		{
			List<Pais> GetListPaisDS = new List<Pais>();

			try
			{
				string sql = "select id_pais, name_pais, capital_pais, flag_pais, population_pais, region_pais, pais_fav from ListaPais";

				command = new SQLiteCommand(sql, connection);

				//Lê cada registo
				SQLiteDataReader reader = command.ExecuteReader();
			
				while (reader.Read())
				{
					GetListPaisDS.Add(new Pais
					{
						idxPK = (int)reader["id_pais"],
						name = (string)reader["name_pais"],
						capital = (string)reader["capital_pais"],
						flag = (string)reader["flag_pais"],
						population = (int)reader["population_pais"],
						region = (string)reader["region_pais"],
						currencies = getCurrency(reader["id_pais"].ToString()),
						topLevelDomain = getDomains(reader["id_pais"].ToString()),
						favorite = (int)reader["pais_fav"],
					});
				}

				connection.Close();

				return GetListPaisDS;
			}
			catch (Exception ex)
			{
				dialogService.ShowMessage("Erro: DS_03", ex.Message);
				return null;
			}
		}

		public List<Currency> getCurrency(string idpais) //Metodo auxiliar que coloca a lista currencies na base de dados.
		{
			List<Currency> ListGetCurrency = new List<Currency>();

			try
			{
				string sqlCur = "select code_moeda, name_moeda, symbol_moeda from ListaMoeda where id_pais=" + idpais;

				command = new SQLiteCommand(sqlCur, connection);

				SQLiteDataReader readerCur = command.ExecuteReader();

				while (readerCur.Read())
				{
					ListGetCurrency.Add(new Currency
					{
						code = (string)readerCur["code_moeda"], 
						name = (string)readerCur["name_moeda"],
						symbol = (string)readerCur["symbol_moeda"],
					});
				}

				return ListGetCurrency;
			}
			catch (Exception ex)
			{
				dialogService.ShowMessage("Erro: DS_05", ex.Message);
				return null;
			}
		}

		public List<string> getDomains(string idpais) //Metodo auxiliar que coloca a lista topLevelDomain na base de dados.
		{
			List<string> ListGetDomains = new List<string>();

			try
			{
				string sqlDom = "select dominio from ListaDominio where id_pais=" + idpais;

				command = new SQLiteCommand(sqlDom, connection);

				SQLiteDataReader readerDom = command.ExecuteReader();

				while (readerDom.Read())
				{
					ListGetDomains.Add((string)readerDom["dominio"]);
				}

				return ListGetDomains;
			}
			catch (Exception ex)
			{
				dialogService.ShowMessage("Erro: DS_06", ex.Message);
				return null;
			}



		}

		public void DeleteData() //Apaga os dados das tabelas da base de dados.
		{
			try
			{
				string sql = "delete from ListaPais";
				command = new SQLiteCommand(sql, connection);
				command.ExecuteNonQuery();

				string sql01 = "delete from ListaMoeda";
				command = new SQLiteCommand(sql01, connection);
				command.ExecuteNonQuery();

				string sql02 = "delete from ListaDominio";
				command = new SQLiteCommand(sql02, connection);
				command.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				dialogService.ShowMessage("Erro: DS_04", ex.Message);
			}
		}
	}
}
